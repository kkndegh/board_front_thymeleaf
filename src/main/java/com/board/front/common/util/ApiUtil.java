package com.board.front.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiUtil {

    @Value("${api.host}")
    private String apiHost;
    
    /**
     * HTTP프로토콜 통신 메서드
     * 
     * @param HttpParam
     * @return String
     */
    public String getApi(String addr, String method, String reqData) {
        HttpURLConnection conn = null;
        String rspStr = "";

        try {
            // URL 설정
            URL url = new URL(apiHost + addr);

            conn = (HttpURLConnection) url.openConnection();

            // methodType의 경우 POST, GET, PUT, DELETE 가능
            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            if (!method.equals("GET")) {
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream(), "UTF-8"));
                bw.write(reqData);
                bw.flush();
                bw.close();
            }

            // 보내고 결과값 받기
            int responseCode = conn.getResponseCode();
            if (responseCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                rspStr = sb.toString();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return rspStr;
    }
}