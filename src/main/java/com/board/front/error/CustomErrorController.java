package com.board.front.error;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.boot.web.servlet.error.ErrorController;

@Controller
public class CustomErrorController implements ErrorController {

    private static final String PATH = "/error/error";

    @RequestMapping(PATH)
    public ModelAndView handleError(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        modelAndView.addObject("code", response.getStatus());
        modelAndView.addObject("message", getErrorMessage(request));
        return modelAndView;
    }

    private String getErrorMessage(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        //Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        return statusCode != null ? statusCode.toString() : "Unknown error occurred";
    }
}