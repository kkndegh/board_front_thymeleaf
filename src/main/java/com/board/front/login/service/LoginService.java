package com.board.front.login.service;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.board.front.common.util.ApiUtil;
import com.board.front.login.model.UserModel;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class LoginService {

    public final ApiUtil apiUtil;

	public void loginProc(UserModel userModel, HttpServletResponse response) {
        try {
            String addr = "/api/user/get";

            ObjectMapper mapper = new ObjectMapper();
            String resStq = mapper.writeValueAsString(userModel);

            //HTTP REQ, RES
            String resStr = apiUtil.getApi(addr, "POST", resStq);

            //RES 타입변환( JSON -> MAP )
            TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String,Object>>() {};
            Map<String, Object> resObj = mapper.readValue(resStr, typeReference);
            Map<String, Object> resDataObj = mapper.readValue(resObj.get("data").toString(), typeReference);

            if(resDataObj.isEmpty()){
                // HttpServletResponse 객체를 사용하여 응답 헤더를 설정합니다.
                response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", "/login");
            }else{
                
            }

        } catch (Exception e) {
            log.error("로그인 에러발생 :: " + e.toString());
        }
    }

	public void registerProc(UserModel userModel, HttpServletResponse response) {
        try {
            String addr = "/api/user/add";

            ObjectMapper mapper = new ObjectMapper();
            String resStq = mapper.writeValueAsString(userModel);

            //HTTP REQ, RES
            String resStr = apiUtil.getApi(addr, "POST", resStq);

            //RES 타입변환( JSON -> MAP )
            TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String,Object>>() {};
            Map<String, Object> resObj = mapper.readValue(resStr, typeReference);

            if(resObj.get("rstCode").equals(200)){
                // HttpServletResponse 객체를 사용하여 응답 헤더를 설정합니다.
                response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", "/login");
            }
        } catch (Exception e) {
            log.error("회원가입 에러발생 :: " + e.toString());
        }
	}
}