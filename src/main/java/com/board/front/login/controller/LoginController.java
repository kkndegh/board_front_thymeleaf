package com.board.front.login.controller;

import javax.servlet.http.HttpServletResponse;

import com.board.front.login.model.UserModel;
import com.board.front.login.service.LoginService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller
public class LoginController {

    public final LoginService loginService;
    
    /**
     * 로그인 페이지
     * 
     * @return String
     */
    @GetMapping("/login")
    public String login(){
        return "view/login/login";
    }
    
    /**
     * 로그인 프로세스
     * 
     * @param userModel
     * @param response
     */
    @PostMapping("/login")
    public void loginProc(UserModel userModel, HttpServletResponse response){
        loginService.loginProc(userModel, response);
    }

    /**
     * 회원가입 페이지
     * 
     * @return String
     */
    @GetMapping("/register")
    public String register(ModelMap modelMap){
        return "view/login/register";
    }

    /**
     * 회원가입 프로세스
     * 
     * @param userModel
     * @param response
     */
    @PostMapping("/register")
    public void registerProc(UserModel userModel, HttpServletResponse response){
        loginService.registerProc(userModel, response);
    }
}