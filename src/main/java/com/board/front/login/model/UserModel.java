package com.board.front.login.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserModel implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private String email;
    private String passwd;
    private String name;
    private int age;
}